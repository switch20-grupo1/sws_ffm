# Add Invoice

# 1. Requirements

## 1.1 System Sequence Diagram

# 2. Analysis

## 2.1

## 2.2 Domain Model Excerpt

# 3. Design

## 3.1. Functionality Development

The Sequence Diagram for this user story:

```plantuml
@startuml

header SD
title Add an Invoice
autonumber
participant ": App" as App
participant ": InvoiceRepository" as Rep
participant "invoice\n: Invoice" as Invoice

[o-> App : newInvoice(invoiceId,description,value,\ndate,supplierVat,clientVat,categoryId)
activate App
App -> Rep : newInvoice(invoiceId,description,value,\ndate,supplierVat,clientVat,categoryId)
activate Rep
Rep --> Invoice** : create(invoiceId,description,value,\n date,supplierVat,clientVat,categoryId)
'activate Invoice
Invoice --> "invoiceId\n:InvoiceId"** : create()
Invoice --> "description\n:Description"** : create()
Invoice --> "value\n:Value"** : create()
Invoice --> "date\n:Date"** : create()
Invoice --> "supplierVat\n:SupplierVat"** : create()
Invoice --> "clientVat\n:ClientVat"** : create()
Invoice --> "categoryId\n:CategoryId"** : create()
note over Invoice : Do we return this object to\nInvoice Repository?
'return invoice
Rep -> Rep : addToList(invoice)
return invoiceId
note left: The ID is generated \nupon Invoice creation?
return invoiceId
note right: Do we return something\n to the outside?

@enduml
```

## 3.2. Class Diagram

```plantuml
@startuml
title Class Diagram Add Invoice
hide empty members

package "Invoice" {
 class Invoice <<Entity>> {
 }
}
@enduml
```

## 3.3. Applied Patterns

## 3.4. Tests

### 3.4.1 Unit Tests

  **Invoice Repository**

  **Positive Test Case:** Create an invoice successfully

```java
    @Test
    void addNewInvoiceSuccessfully() {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();
        int expected = 1;
        int result;

        invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId);
        result = invoiceRepository.getInvoicesSize();

        assertEquals(expected, result);
    }
```

  **Negative Test Case:** Try to create an invoice with invalid data

```java
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"---"})
    void tryToCreateInvoiceInvalidInvoiceId(String candidate) {
        String invoiceId = candidate;
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidDescription(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = candidate;
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"EUR0.00", "XYZ12.34", "EUR-20.00"})
    void tryToCreateInvoiceInvalidValue(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = candidate;
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"2025-01-03"})
    void tryToCreateInvoiceInvalidDate(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = candidate;
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidSupplierVat(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = candidate;
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidClientVat(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = candidate;
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"AnEmailAccountWithoutAtSymbol.com"})
    void tryToCreateInvoiceInvalidClientId(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = candidate;
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidCategoryId(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = candidate;
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }
```

  **Invoice**

  **Positive Test Case:** Create an invoice successfully

```java
    @Test
    void createInvoiceSuccessfully() {
        InvoiceId invoiceId = new InvoiceId("FS AAA026/078323");
        Description description = new Description("Modelo Continente Hipermercados S A");
        Value value = new Value("EUR59.93");
        InvoiceDate date = new InvoiceDate("2021-01-03");
        SupplierVat supplierVat = new SupplierVat("502011475");
        PersonVat clientVat = new PersonVat("123456789");
        Email clientId = new Email("test@test.com");
        CategoryId categoryId = new CategoryId("Shopping");

        Invoice invoice = new Invoice(invoiceId,description,value,date,supplierVat,clientVat,clientId,categoryId);

        assertNotNull(invoice);
    }
```

  **Negative Test Case:** Try to create an invoice with invalid data

```java
    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"---"})
    void tryToCreateInvoiceInvalidInvoiceId(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new InvoiceId(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidDescription(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new Description(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"EUR0.00", "XYZ12.34", "EUR-20.00"})
    void tryToCreateInvoiceInvalidValue(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new Value(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"2025-01-03"})
    void tryToCreateInvoiceInvalidDate(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new InvoiceDate(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidSupplierVat(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new SupplierVat(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidClientVat(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new PersonVat(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"AnEmailAccountWithoutAtSymbol.com"})
    void tryToCreateInvoiceInvalidClientId(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new Email(candidate));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidCategoryId(String candidate) {
        assertThrows(IllegalArgumentException.class, () -> new CategoryId(candidate));
    }
```

# 4. Implementation

# 5. Integration/Demonstration

# 6. Observations