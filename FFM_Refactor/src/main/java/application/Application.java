package application;

import model.shared.Email;
import repositories.FamilyRepository;
import repositories.FamilyRepository;
import repositories.InvoiceRepository;
import repositories.PersonRepository;

import java.time.DateTimeException;
import java.time.LocalDate;

public class Application {
    private final PersonRepository personRepository;
    private final InvoiceRepository invoiceRepository;
    private final FamilyRepository familyRepository;

    public Application() {
        this.personRepository = new PersonRepository();
        this.invoiceRepository = new InvoiceRepository();
        this.familyRepository = new FamilyRepository();
    }

    public Email addPerson(String email, String personName, String vat, String street, String location, String postalCode, LocalDate birthDate, String phoneNumber) {
        Email emailId;
        try {
            emailId = personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        } catch (IllegalArgumentException | DateTimeException exception) {
            // verificar o que devemos fazer neste caso
            throw new IllegalArgumentException("Invalid data.");
        }
        return emailId;
    }

    public void newInvoice(String invoiceId, String description, String value, String date, String supplierVat, String clientVat, String clientId, String categoryId) {
        invoiceRepository.newInvoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId);
    }

}
