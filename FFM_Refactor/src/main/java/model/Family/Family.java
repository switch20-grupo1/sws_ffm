package model.Family;

import model.interfaces.AggregateRoot;
import model.shared.Email;
import model.shared.FamilyId;
import model.shared.FamilyName;
import model.shared.RegistrationDate;

import java.util.Objects;

public class Family implements AggregateRoot {
    private FamilyId id;
    private FamilyName name;
    private RegistrationDate registrationDate;
    private Email adminId;

    public Family(int numberOfFamilies, String name, String email) {
        FamilyId id1 = new FamilyId(numberOfFamilies);
        FamilyName familyName = new FamilyName(name);
        RegistrationDate registrationDate1 = new RegistrationDate();
        Email adminEmail = new Email(email);

        if (validateFamily(id1, familyName, registrationDate1, adminEmail)) {
            this.id = id1;
            this.name = familyName;
            this.registrationDate = registrationDate1;
            this.adminId = adminEmail;
        } else {
            throw new IllegalArgumentException("Invalid arguments");
        }
    }

    private boolean validateFamily(FamilyId id, FamilyName familyName, RegistrationDate registrationDate, Email adminEmail) {
        boolean result;
        result = validateId(id) && validateName(familyName) && validateRegistrationDate(registrationDate) && validateEmail(adminEmail);
        return result;
    }

    private boolean validateId(FamilyId id) {
        boolean result;
        result = id != null;
        return result;
    }

    private boolean validateName(FamilyName familyName) {
        boolean result;
        result = familyName != null;
        return result;
    }

    private boolean validateRegistrationDate(RegistrationDate registrationDate) {
        boolean result;
        result = registrationDate != null;
        return result;
    }

    private boolean validateEmail(Email adminEmail) {
        boolean result;
        result = adminEmail != null;
        return result;
    }

    public FamilyId getId() {
        return id;
    }

    public boolean isSameFamily(FamilyId familyId){
        boolean result = false;
        if(this.id == familyId){
            result = true;
        }
        return result;
    }

    public FamilyName getName() {
        return name;
    }

    public RegistrationDate getRegistrationDate() {
        return registrationDate;
    }

    public Email getAdminId(){return adminId;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(id, family.id) && Objects.equals(name, family.name) && Objects.equals(registrationDate, family.registrationDate) && Objects.equals(adminId, family.adminId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, registrationDate, adminId);
    }
}
