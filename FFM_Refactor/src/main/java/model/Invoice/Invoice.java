package model.Invoice;

import model.interfaces.AggregateRoot;
import model.shared.*;

public class Invoice implements AggregateRoot {
    private final InvoiceId invoiceId;
    private final Description description;
    private final Value value;
    private final InvoiceDate invoiceDate;
    private final SupplierVat supplierVat;
    private final PersonVat clientVat;
    private final Email clientId;
    private final CategoryId category;

    public Invoice(String invoiceId, String description, String value, String invoiceDate, String supplierVat, String clientVat, String clientId, String categoryId) {
        this.invoiceId = new InvoiceId(invoiceId);
        this.description = new Description(description);
        this.value = new Value(value);
        this.invoiceDate = new InvoiceDate(invoiceDate);
        this.supplierVat = new SupplierVat(supplierVat);
        this.clientVat = new PersonVat(clientVat);
        this.clientId = new Email(clientId);
        this.category = new CategoryId(categoryId);
    }
}
