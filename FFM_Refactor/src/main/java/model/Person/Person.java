package model.Person;

import model.interfaces.AggregateRoot;
import model.shared.*;

import java.time.LocalDate;

public class Person implements AggregateRoot {
    private final Email id;
    private final PersonName name;
    private final Address address;
    private final BirthDate birthdate;
    private final PersonVat vat;
    private PhoneNumber phoneNumber;

    public Person(String email, String personName, String vat, String street, String location, String postalCode, LocalDate birthDate, String phoneNumber) {
        this.id = new Email(email);
        this.name = new PersonName(personName);
        this.vat = new PersonVat(vat);
        this.address = new Address(street, location, postalCode);
        this.birthdate = new BirthDate(birthDate);
        this.phoneNumber = new PhoneNumber(phoneNumber);
    }

    public Person(String email, String personName, String vat, String street, String location, String postalCode, LocalDate birthDate) {
        this.id = new Email(email);
        this.name = new PersonName(personName);
        this.vat = new PersonVat(vat);
        this.address = new Address(street, location, postalCode);
        this.birthdate = new BirthDate(birthDate);
    }

    public Email getId() {
        return this.id;
    }

}
