package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class Address implements ValueObject {
    private final String street;
    private final String location;
    private final String postalCode;

    public Address(String street, String location, String postalCode) {
        if (!validateStreet(street)) {
            throw new IllegalArgumentException("Invalid Street address");
        }
        this.street = street;

        if (!validatePostalCode(postalCode)) {
            throw new IllegalArgumentException("Invalid Postal Code");
        }
        this.postalCode = postalCode;

        if (!validateLocation(location)) {
            throw new IllegalArgumentException("Invalid Location");
        }
        this.location = location;
    }

    private boolean validateStreet(String street) {
        if (street == null || street.trim().length() == 0 || street.isEmpty()) {
            return false;
        }
        return checkFormatStreet(street);
    }

    private boolean checkFormatStreet(String street) {

        String streetRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s,]+[0-9]*$"; //with spaces and special chars

        Pattern pat = Pattern.compile(streetRegex);
        return pat.matcher(street).matches();
    }

    private boolean validateLocation(String location) {
        if (location == null || location.trim().length() == 0 || location.isEmpty()) {
            return false;
        }

        return checkFormatLocation(location);
    }

    private boolean checkFormatLocation(String location) {

        String locationRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s]*$"; //with spaces and special chars

        Pattern pat = Pattern.compile(locationRegex);
        return pat.matcher(location).matches();
    }

    private boolean validatePostalCode(String postalCode) {
        if (postalCode == null || postalCode.trim().length() == 0 || postalCode.isEmpty()) {
            return false;
        }

        return checkFormatPostalCode(postalCode);
    }

    private boolean checkFormatPostalCode(String postalCode) {

        String postalCodeRegex = "^[1-9][0-9]{3}(?:-[0-9]{3})$";

        Pattern pat = Pattern.compile(postalCodeRegex);
        return pat.matcher(postalCode).matches();
    }
}
