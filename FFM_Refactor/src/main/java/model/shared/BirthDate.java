package model.shared;

import model.interfaces.ValueObject;

import java.time.DateTimeException;
import java.time.LocalDate;

public class BirthDate implements ValueObject {
    private final LocalDate birthDate;

    public BirthDate(LocalDate birthDate) {
        if (!validateBirthDate(birthDate)) {
            throw new DateTimeException("The birthDate is not valid.");
        }
        this.birthDate = birthDate;
    }

    private boolean validateBirthDate(LocalDate birthDate) {
        return birthDate != null && !birthDate.isAfter(LocalDate.now());
    }
}
