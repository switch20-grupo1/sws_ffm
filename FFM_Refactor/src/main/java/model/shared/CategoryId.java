package model.shared;

import model.interfaces.Id;

public class CategoryId implements Id {
    private final String id;

    public CategoryId(String id) {
        this.id = id;
        validate();
    }

    private void validate() {
        validateString();
    }

    private void validateString() {
        if (this.id == null || this.id.isEmpty() || this.id.isBlank()) {
            throw new IllegalArgumentException("Description is not valid.");
        }
    }
}
