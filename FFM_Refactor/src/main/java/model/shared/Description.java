package model.shared;

public class Description {
    private final String description;

    public Description(String description) {
        this.description = description;
        validate();
    }

    private void validate() {
        validateString();
    }

    private void validateString() {
        if (this.description == null || this.description.isEmpty() || this.description.isBlank()) {
            throw new IllegalArgumentException("Description is not valid.");
        }
    }
}
