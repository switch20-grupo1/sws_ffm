package model.shared;

import model.interfaces.Id;

import java.util.Objects;
import java.util.regex.Pattern;

public class Email implements Id {
    private final String emailAddress;

    public Email(String emailAddress) {
        this.emailAddress = emailAddress;
        validate();
    }

    private void validate() {
        validateString();
        validateFormat();
    }

    private void validateString() {
        if (this.emailAddress == null || this.emailAddress.isEmpty() || this.emailAddress.isBlank()) {
            throw new IllegalArgumentException("E-mail Address is not valid.");
        }
    }

    private void validateFormat() {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";
        Pattern pattern = Pattern.compile(emailRegex);
        if (!pattern.matcher(this.emailAddress).matches()) {
            throw new IllegalArgumentException("E-mail Address doesn't have a valid format.");
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(emailAddress);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Email email = (Email) o;
        return Objects.equals(emailAddress, email.emailAddress);
    }

}
