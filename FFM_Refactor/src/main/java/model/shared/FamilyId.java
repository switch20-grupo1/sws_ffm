package model.shared;

import model.interfaces.Id;

import java.util.Objects;

public class FamilyId implements Id {
    private final int id;

    public FamilyId(int numberOfFamilies) {
        int newId;
        if(validateNumberOfFamilies(numberOfFamilies)) {
            newId = numberOfFamilies + 1;
            this.id = newId;
        } else {
            throw new IllegalArgumentException("The number of families cannot be less than zero");
        }
    }

    private boolean validateNumberOfFamilies(int numberOfFamilies) {
        boolean result = true;
        if (numberOfFamilies < 0){
            result = false;
        }
        return result;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FamilyId familyId = (FamilyId) o;
        return id == familyId.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
