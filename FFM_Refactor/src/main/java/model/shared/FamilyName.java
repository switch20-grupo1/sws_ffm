package model.shared;

import model.interfaces.ValueObject;

import java.util.Objects;
import java.util.regex.Pattern;

public class FamilyName implements ValueObject {
    String name;

    public FamilyName(String name) {
        boolean nameValidation = checkFormat(name);
        if (nameValidation) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Invalid name");
        }
    }

    private boolean checkFormat(String name) {
        boolean result;
        if (name == null || name.isEmpty()) {
            result = false;
        } else {
            result = validateName(name);
        }
        return result;
    }

    private boolean validateName(String name) {
        String alphaRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s]*$"; //with spaces and special chars

        Pattern pat = Pattern.compile(alphaRegex);
        return pat.matcher(name).matches();
    }

    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FamilyName that = (FamilyName) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
