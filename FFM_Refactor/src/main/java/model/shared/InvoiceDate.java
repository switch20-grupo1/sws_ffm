package model.shared;

import model.interfaces.ValueObject;

import java.time.LocalDate;

public class InvoiceDate implements ValueObject {
    private final LocalDate date;

    public InvoiceDate(LocalDate date) {
        this.date = date;
        validate();
    }

    public InvoiceDate(String date) {
        assertStringNotNull(date);
        this.date = LocalDate.parse(date);
        validate();
    }

    private static void assertStringNotNull(String dateString) {
        if (dateString == null || dateString.isEmpty() || dateString.isBlank()) {
            throw new IllegalArgumentException("Date is not valid.");
        }
    }

    private void validate() {
        validateDateAfterCurrentDay();
    }

    private void validateDateAfterCurrentDay() {
        LocalDate today = LocalDate.now();
        if (this.date.isAfter(today)) {
            throw new IllegalArgumentException("Invoice Date is in the future");
        }
    }
}
