package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class InvoiceId implements ValueObject {
    private final String invoiceId;

    public InvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
        validate();
    }

    private void validate() {
        validateString();
        validateFormat();
    }

    private void validateString() {
        if (this.invoiceId == null || this.invoiceId.isEmpty() || this.invoiceId.isBlank()) {
            throw new IllegalArgumentException("Invoice ID is not valid.");
        }
    }

    private void validateFormat() {
        String invoiceRegex = "^[a-zA-Z0-9 /]*$";
        Pattern pattern = Pattern.compile(invoiceRegex);
        if (!pattern.matcher(this.invoiceId).matches()) {
            throw new IllegalArgumentException("Invoice ID doesn't have a valid format.");
        }
    }
}
