package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class PersonName implements ValueObject {
    private final String personName;

    public PersonName(String name) {
        if (!validatePersonName(name)) {
            throw new IllegalArgumentException("Invalid name");
        }
        this.personName = name;
    }

    private boolean validatePersonName(String name) {
        if (name == null || name.isEmpty() || name.trim().length() == 0) {
            return false;
        }
        return checkFormat(name);
    }

    private boolean checkFormat(String name) {
        String alphaRegex = "^[-'a-zA-ZÀ-ÖØ-öø-ÿ\\s]*$"; //with spaces and special chars

        Pattern pat = Pattern.compile(alphaRegex);
        return pat.matcher(name).matches();
    }
}

