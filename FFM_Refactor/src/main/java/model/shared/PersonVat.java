package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class PersonVat implements ValueObject {
    public final String vat;

    public PersonVat(String vat) {
        this.vat = vat;
        validate();
    }

    private void validate() {
        validateString();
        validateFormat();
    }

    private void validateString() {
        if (this.vat == null || this.vat.isEmpty() || this.vat.isBlank()) {
            throw new IllegalArgumentException("VAT is not valid.");
        }
    }

    private void validateFormat() {
        String vatRegex = "^[123][0-9]{8}$";
        Pattern pattern = Pattern.compile(vatRegex);
        if (!pattern.matcher(this.vat).matches()) {
            throw new IllegalArgumentException("VAT doesn't have a valid format.");
        }
    }
}
