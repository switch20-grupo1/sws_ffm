package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class PhoneNumber implements ValueObject {
    private final String phoneNumber;

    public PhoneNumber(String phoneNumber) {
        if (!validatePhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("This phone number is not valid");
        }
        this.phoneNumber = phoneNumber;

    }

    private boolean validatePhoneNumber(String phoneNumber) {
        if (phoneNumber == null) {
            return false;
        }

        return checkFormat(phoneNumber);
    }

    private boolean checkFormat(String phoneNumber) {
        String phoneNumberRegex = "^9[1236][0-9]{7}$|^2[1-9][0-9]{7}$";

        Pattern pat = Pattern.compile(phoneNumberRegex);
        return pat.matcher(phoneNumber).matches();
    }

    @Override
    public String toString() {
        return phoneNumber;
    }
}
