package model.shared;

import model.interfaces.ValueObject;

import java.time.LocalDate;
import java.util.Objects;

public class RegistrationDate implements ValueObject {
    LocalDate date;

    public RegistrationDate(){
        LocalDate today = LocalDate.now();
        this.date = today;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationDate that = (RegistrationDate) o;
        return Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date);
    }
}
