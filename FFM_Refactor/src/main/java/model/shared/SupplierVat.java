package model.shared;

import model.interfaces.ValueObject;

import java.util.regex.Pattern;

public class SupplierVat implements ValueObject {
    private final String supplierVat;

    public SupplierVat(String supplierVat) {
        this.supplierVat = supplierVat;
        validate();
    }

    private void validate() {
        validateString();
        validateFormat();
    }

    private void validateString() {
        if (this.supplierVat == null || this.supplierVat.isEmpty() || this.supplierVat.isBlank()) {
            throw new IllegalArgumentException("VAT is not valid.");
        }
    }

    private void validateFormat() {
        String supplierVatRegex = "^[12356][0-9]{8}$";
        Pattern pattern = Pattern.compile(supplierVatRegex);
        if (!pattern.matcher(this.supplierVat).matches()) {
            throw new IllegalArgumentException("VAT doesn't have a valid format.");
        }
    }
}
