package model.shared;

import model.interfaces.ValueObject;

public class Value implements ValueObject {
    private final String currency;
    private final double value;

    public Value(String currency, double value) {
        this.currency = currency;
        this.value = value;
        validate();
    }

    public Value(String string) {
        validateString(string);
        this.currency = string.substring(0, 3);
        this.value = Double.parseDouble(string.substring(3));
        validate();
    }

    private void validate() {
        validateCurrency();
        validateValue();
    }

    private void validateString(String string) {
        if (string == null || string.isEmpty() || string.isBlank()) {
            throw new IllegalArgumentException("Description is not valid.");
        }
    }

    private void validateCurrency() {
        if (!this.currency.equals(("EUR"))) {
            throw new IllegalArgumentException("Currency is not valid");
        }
    }

    private void validateValue() {
        if (this.value <= 0) {
            throw new IllegalArgumentException("Value is not valid");
        }
    }
}
