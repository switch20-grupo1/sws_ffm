package repositories;

import model.Family.Family;
import model.shared.FamilyId;

import java.util.ArrayList;
import java.util.List;

public class FamilyRepository {
    private List<Family> families;

    public FamilyRepository(){
        families = new ArrayList<>();
    }

    public int getFamiliesSize(){
        return families.size();
    }

    public FamilyId createFamily(String name, String adminId){
        int numberOfFamilies = getFamiliesSize();

        Family family = new Family(numberOfFamilies, name, adminId);
        families.add(family);

        return family.getId();
    }

    public Family getFamilyById(FamilyId familyId){
        Family resultantFamily = null;

        for (Family family: families) {
            if(family.isSameFamily(familyId)){
                resultantFamily = family;
            }
        }

        return resultantFamily;
    }
}
