package repositories;

import model.Invoice.Invoice;

import java.util.ArrayList;
import java.util.List;

public class InvoiceRepository {
    private final List<Invoice> invoices;

    public InvoiceRepository() {
        this.invoices = new ArrayList<>();
    }

    public void newInvoice(String invoiceId, String description, String value, String date, String supplierVat, String clientVat, String clientId, String categoryId) {
        Invoice invoice = new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId);
        invoices.add(invoice);
    }

    public int getInvoicesSize() {
        return invoices.size();
    }
}
