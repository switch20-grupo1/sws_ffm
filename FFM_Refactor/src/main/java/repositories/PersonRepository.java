package repositories;

import model.Person.Person;
import model.shared.Email;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {
    private final List<Person> people;

    public PersonRepository() {
        this.people = new ArrayList<>();
    }


    public Email createPerson(String email, String personName, String personVat, String street, String location, String postalCode, LocalDate birthDate, String phoneNumber) {
        validate(email);
        Person aPerson;

        if (phoneNumber.isEmpty() || phoneNumber.isBlank()) {
            aPerson = new Person(email, personName, personVat, street, location, postalCode, birthDate);
        } else {
            aPerson = new Person(email, personName, personVat, street, location, postalCode, birthDate, phoneNumber);
        }
        people.add(aPerson);

        return aPerson.getId();
    }

    private void validate(String email) {
        if (existsEmail(email)) {
            throw new IllegalArgumentException("Person with this email already exists!");
        }
    }

    private boolean existsEmail(String email) {
        boolean exists = false;
        Email anEmail = new Email(email);
        for (int i = 0; i < people.size() && !exists; i++) {
            exists = (people.get(i).getId().equals(anEmail));
        }
        return exists;
    }

}
