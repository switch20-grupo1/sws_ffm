package application;

import model.shared.Email;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ApplicationTest {

    @Test
    void ensureApplicationNotNull() {
        Application application = new Application();
        assertNotNull(application);
    }

    @Test
    void ensurePersonIsAdded() {
        //arrange
        Application application = new Application();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        Email result;

        //act
        result = application.addPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);
    }

    @Test
    void ensurePersonIsAddedWithEmptyPhoneNumber() {
        //arrange
        Application application = new Application();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "";
        Email result;

        //act
        result = application.addPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);
    }

    @Test
    void ensurePersonNotAddedWhenInvalidEmail() {
        //arrange
        Application application = new Application();
        String email = "antonio.gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> application.addPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonNotAddedWhenInvalidBirthDate() {
        //arrange
        Application application = new Application();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = null;
        String phoneNumber = "916175678";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> application.addPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonNotAddedWhenPersonAlreadyExists() {
        //arrange
        Application application = new Application();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        application.addPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        String sameEmail = "antonio@gmail.com";
        String otherPersonName = "António";
        String otherVat = "229654117";
        String otherStreet = "Rua da Alegria";
        String otherLocation = "Porto";
        String otherPostalCode = "4679-876";
        LocalDate otherBirthDate = LocalDate.of(1994, Month.NOVEMBER, 1);
        String otherPhoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> application.addPerson(sameEmail, otherPersonName, otherVat, otherStreet, otherLocation, otherPostalCode, otherBirthDate, otherPhoneNumber));
    }

}
