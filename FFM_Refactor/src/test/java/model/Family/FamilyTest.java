package model.Family;

import model.shared.Email;
import model.shared.FamilyId;
import model.shared.FamilyName;
import model.shared.RegistrationDate;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void createAFamily_Successfully() {
        FamilyId id = new FamilyId(0);
        RegistrationDate today = new RegistrationDate();
        FamilyName name = new FamilyName("Silva");
        Email email = new Email("antonio@gmail.com");

        Family result = new Family(0, "Silva", "antonio@gmail.com");

        assertNotNull(result);
        assertEquals(id, result.getId());
        assertEquals(today, result.getRegistrationDate());
        assertEquals(name, result.getName());
        assertEquals(email, result.getAdminId());
    }

    @Test
    void createAFamily_Unsuccessfully_NullId() {
        Integer lastId = null;

        assertThrows(NullPointerException.class, () -> new Family(lastId, "Silva","antonio@gmail.com" ));
    }

    @Test
    void createAFamily_Unsuccessfully_NullName() {
        assertThrows(IllegalArgumentException.class, () -> new Family(0, null, "antonio@gmail.com"));
    }

    @Test
    void createAFamily_Unsuccessfully_NullEmail(){
        assertThrows(IllegalArgumentException.class, () -> new Family(0, "Silva", null));
    }
}