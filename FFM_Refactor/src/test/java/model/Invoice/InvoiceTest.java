package model.Invoice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import repositories.InvoiceRepository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InvoiceTest {

    @Test
    void createInvoiceSuccessfully() {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";

        Invoice invoice = new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId);

        assertNotNull(invoice);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"---"})
    void tryToCreateInvoiceInvalidInvoiceId(String candidate) {
        String invoiceId = candidate;
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidDescription(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = candidate;
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"EUR0.00", "XYZ12.34", "EUR-20.00"})
    void tryToCreateInvoiceInvalidValue(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = candidate;
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"2025-01-03"})
    void tryToCreateInvoiceInvalidDate(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = candidate;
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidSupplierVat(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = candidate;
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"777777777"})
    void tryToCreateInvoiceInvalidClientVat(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = candidate;
        String clientId = "test@test.com";
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"AnEmailAccountWithoutAtSymbol.com"})
    void tryToCreateInvoiceInvalidClientId(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = candidate;
        String categoryId = "Shopping";
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void tryToCreateInvoiceInvalidCategoryId(String candidate) {
        String invoiceId = "FS AAA026/078323";
        String description = "Modelo Continente Hipermercados S A";
        String value = "EUR59.93";
        String date = "2021-01-03";
        String supplierVat = "502011475";
        String clientVat = "123456789";
        String clientId = "test@test.com";
        String categoryId = candidate;
        InvoiceRepository invoiceRepository = new InvoiceRepository();

        assertThrows(IllegalArgumentException.class, () -> new Invoice(invoiceId, description, value, date, supplierVat, clientVat, clientId, categoryId));
    }
}