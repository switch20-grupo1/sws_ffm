package model.Person;

import model.shared.Email;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    @Test
    void ensurePersonIsCreated() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        Person result;

        //act
        result = new Person(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);
    }

    @Test
    void ensurePersonIsCreatedWithoutPhoneNumber() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        Person result;

        //act
        result = new Person(email, personName, vat, street, location, postalCode, birthDate);

        //assert
        assertNotNull(result);
    }

    @Test
    void ensureGetId() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        Person aPerson = new Person(email, personName, vat, street, location, postalCode, birthDate);
        Email expected = new Email(email);
        Email result;

        //act
        result = aPerson.getId();

        //assert
        assertEquals(expected, result);
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidEmail() {
        //arrange
        String invalidEmail = "antonio@gmail.c";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(invalidEmail, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidName() {
        //arrange
        String email = "antonio@gmail.com";
        String invalidPersonName = "António1234";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, invalidPersonName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidVAT() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "2296541179";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidStreetFormat() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "229654117";
        String street = "";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidLocationFormat() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto12345";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidPostalCode() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-87634567";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidBirthDate() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = null;
        String phoneNumber = "916789456";

        //act + assert
        assertThrows(DateTimeException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWithInvalidPhoneNumber() {
        //arrange
        String email = "antonio@gmail.com";
        String personName = "António";
        String invalidVat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "1917864567";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> new Person(email, personName, invalidVat, street, location, postalCode, birthDate, phoneNumber));
    }

}
