package model.shared;

import model.shared.Address;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class AddressTest {

    @Test
    public void CreatingValidStreetAddress() {
        String postalCode = "4000-123";
        String location = "Porto";
        Address street = new Address("Rua da Cruz", location, postalCode);
        Assertions.assertNotNull(street);
    }

    @Test
    public void CreatingInvalidStreetAddressWithSpecialCharacter() {
        String postalCode = "4000-123";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address street = new Address("Rua da/ Cruz", location, postalCode);
                });
    }

    @Test
    public void ensureNullStreetAddressIsNotCreated() {
        String postalCode = "4000-123";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address street = new Address(null, location, postalCode);
                });
    }

    @Test
    public void ensureBlankStreetAddressIsNotCreated() {
        String postalCode = "4000-123";
        String location = "Porto";
        String blankStreet = "";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address street = new Address(blankStreet, location, postalCode);
                });
    }

    @Test
    public void CreatingValidPostalCode() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Address postalCode = new Address(street, location, "4000-123");
        Assertions.assertNotNull(postalCode);
    }

    @Test
    public void createInvalidPostalCode() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, "0000-000");
                });
    }

    @Test
    public void createInvalidPostalCodeWithLessThanFourDigitsOnTheFirstPart() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, "400-000");
                });
    }

    @Test
    public void createInvalidPostalCodeWithLessThanThreeDigitsOnTheSecondPart() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, "4000-00");
                });
    }

    @Test
    public void createInvalidPostalCodeWithSpecialCharacter() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, "4/000-000");
                });
    }

    @Test
    public void ensureNullPostalCodeIsNotCreated() {
        String street = "Rua da Cruz";
        String location = "Porto";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, null);
                });
    }

    @Test
    public void ensureBlankPostalCodeIsNotCreated() {
        String street = "Rua da Cruz";
        String location = "Porto";
        String blankPostalCode = "";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address postalCode = new Address(street, location, blankPostalCode);
                });
    }

    @Test
    public void CreatingValidLocation() {
        String postalCode = "4000-123";
        String street = "Rua da Cruz";
        Address location = new Address(street,"Porto", postalCode);
        Assertions.assertNotNull(location);
    }

    @Test
    public void CreatingInvalidLocation() {
        String postalCode = "4000-123";
        String street = "Rua da Cruz";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address location = new Address(street, "123", postalCode);
                });
    }

    @Test
    public void CreatingInvalidLocationWithSpecialCharacter() {
        String postalCode = "4000-123";
        String street = "Rua da Cruz";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address location = new Address(street,"Porto/", postalCode);
                });
    }

    @Test
    public void ensureNullLocationIsNotCreated() {
        String postalCode = "4000-123";
        String street = "Rua da Cruz";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address location = new Address(street, null, postalCode);
                });
    }

    @Test
    public void ensureBlankLocationIsNotCreated() {
        String postalCode = "4000-123";
        String street = "Rua da Cruz";
        String blankLocation = "";
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    Address location = new Address(street, blankLocation, postalCode);
                });
    }
}
