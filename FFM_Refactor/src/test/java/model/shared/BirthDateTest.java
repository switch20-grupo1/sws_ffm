package model.shared;

import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BirthDateTest {

    @Test
    void createBirthDateEleventhSeptemberNinetyThree() {
        //Arrange
        LocalDate date = LocalDate.of(1993, 9, 11);
        //Act
        BirthDate birthDate = new BirthDate(date);
        //Assert
        assertNotNull(birthDate);
    }

    @Test
    void createBirthDateNinthNovemberEightyEight() {
        //Arrange
        LocalDate date = LocalDate.of(1988, 11, 9);
        //Act
        BirthDate birthDate = new BirthDate(date);
        //Assert
        assertNotNull(birthDate);
    }

    @Test
    void createBirthDateToday() {
        //Arrange
        LocalDate date = LocalDate.now();
        //Act
        BirthDate birthDate = new BirthDate(date);
        //Assert
        assertNotNull(birthDate);
    }

    @Test
    void createBirthDateTomorrow() {
        //Arrange
        LocalDate date = LocalDate.now().plusDays(1);
        //Assert
        assertThrows(DateTimeException.class, () -> new BirthDate(date));
    }

    @Test
    void createBirthDateInTwoMonths() {
        //Arrange
        LocalDate date = LocalDate.now().plusMonths(2);
        //Assert
        assertThrows(DateTimeException.class, () -> new BirthDate(date));
    }

    @Test
    void createNullBirthDate() {
        //Assert
        assertThrows(DateTimeException.class, () -> new BirthDate(null));
    }
}
