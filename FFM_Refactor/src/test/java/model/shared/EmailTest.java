package model.shared;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmailTest {

    @Test
    void createNewEmailWithNumbersAndDomainWithThreeFields() {
        //Arrange
        String emailAddress = "12345@isep.ipp.pt";
        //Act
        Email newEmail = new Email(emailAddress);
        //Assert
        assertNotNull(newEmail);
    }

    @Test
    void createNewEmailWithLettersAndDomainWithThreeFields() {
        //Arrange
        String emailAddress = "abcdefgh@isep.ipp.pt";
        //Act
        Email newEmail = new Email(emailAddress);
        //Assert
        assertNotNull(newEmail);
    }

    @Test
    void createNewEmailWithDomainWithTwoFields() {
        //Arrange
        String emailAddress = "abcdefgh@gmail.com";
        //Act
        Email newEmail = new Email(emailAddress);
        //Assert
        assertNotNull(newEmail);
    }

    @Test
    void creatingEmailWithNoDomainThrowsException() {
        //Arrange
        String emailAddress = "abcdefgh";

        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email(emailAddress));
    }

    @Test
    void creatingEmailWithWrongDomainThrowsException() {
        //Arrange
        String emailAddress = "abcdefgh@isep.ipp.p";

        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email(emailAddress));
    }

    @Test
    void creatingEmailWithDomainComposedOfNumbersThrowsException() {
        //Arrange
        String emailAddress = "abcdefgh@1234.567.89";

        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email(emailAddress));
    }

    @Test
    void creatingNullEmailThrowsException() {
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email(null));
    }

    @Test
    void creatingEmptyEmailThrowsException() {
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email(""));
    }

    @Test
    void creatingBlankEmailThrowsException() {
        //Assert
        assertThrows(IllegalArgumentException.class, () -> new Email("   "));
    }


}
