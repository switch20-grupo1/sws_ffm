package model.shared;

import model.shared.FamilyId;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyIdTest {

    @Test
    void createFamilyId_Successfully_noFamilies() {
        int expected = 1;

        FamilyId result = new FamilyId(0);

        assertNotNull(result);
        assertEquals(expected, result.getId());
    }

    @Test
    void createFamilyId_Unsuccessfully_WithNull() {
        Integer result = null;

        assertThrows(NullPointerException.class, () -> new FamilyId(result));
    }

    @Test
    void createFamilyId_Unsuccessfully_WithNegativeNumber() {
        int result = -1;

        assertThrows(IllegalArgumentException.class, () -> new FamilyId(result));
    }

}