package model.shared;

import model.shared.FamilyName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class FamilyNameTest {

    @ParameterizedTest
    @ValueSource(strings = {"Silva", "Borges-Silva", "Borges Silva"})
    void createFamilyName_Successfully_noFamilies(String name) {
        String expected = name;

        FamilyName result = new FamilyName(name);

        assertNotNull(result);
        assertEquals(expected, result.getName());
    }

    @ParameterizedTest
    @ValueSource(strings = {"Borges-Silva", "Borges Silva"})
    void createFamilyName_Unsuccessfully_DifferentName(String name) {
        String expected = "Silva";

        FamilyName result = new FamilyName(name);

        assertNotNull(result);
        assertNotEquals(expected, result.getName());
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"Filipe203", "123", "Filipe_Antonio"})
    void createFamilyName_Unsuccessfully_NullName(String name) {
        assertThrows(IllegalArgumentException.class, () -> new FamilyName(name));
    }
}
