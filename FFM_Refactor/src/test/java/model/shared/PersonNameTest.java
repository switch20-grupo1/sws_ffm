package model.shared;

import model.shared.PersonName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersonNameTest {

    @Test
    public void CreatingValidName() {
        PersonName personName = new PersonName("Mangala");
        Assertions.assertNotNull(personName);
    }

    @Test
    public void CreatingInvalidName() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    PersonName personName = new PersonName("123");
                });
    }

    @Test
    public void ensureNullNameIsNotCreated() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    PersonName personName = new PersonName(null);
                });
    }

    @Test
    public void ensureBlankNameIsNotCreated() {
        Throwable exception =
                assertThrows(IllegalArgumentException.class, () -> {
                    PersonName personName = new PersonName("");
                });
    }
}
