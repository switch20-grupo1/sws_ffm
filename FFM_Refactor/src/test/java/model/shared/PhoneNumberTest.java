package model.shared;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PhoneNumberTest {

    @Test
    public void CreatingValidPhoneNumber() {
        PhoneNumber phoneNumber = new PhoneNumber("964054870");
        Assertions.assertNotNull(phoneNumber);
    }

    @Test
    public void CreatingPhoneNumberWithAnInvalidFirstDigit() {
        String invalidPhoneNumber = "191958568";
        assertThrows(IllegalArgumentException.class, () -> new PhoneNumber(invalidPhoneNumber));
    }

    @Test
    public void CreatingPhoneNumberWithAnInvalidSecondDigit() {
        String invalidPhoneNumber = "991958568";

        assertThrows(IllegalArgumentException.class, () -> new PhoneNumber(invalidPhoneNumber));
    }

    @Test
    public void CreatingPhoneNumberWithLessThanNineDigits() {
        String invalidPhoneNumber = "23955662";
        assertThrows(IllegalArgumentException.class, () -> new PhoneNumber(invalidPhoneNumber));
    }

    @Test
    public void ensureNullPhoneNumberIsNotCreated() {
        assertThrows(IllegalArgumentException.class, () -> new PhoneNumber(null));
    }
}
