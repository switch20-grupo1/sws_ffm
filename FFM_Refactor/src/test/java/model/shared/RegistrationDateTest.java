package model.shared;

import model.shared.RegistrationDate;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class RegistrationDateTest {

    @Test
    void createRegistrationDate_Successfully_SameDate(){
        LocalDate expected = LocalDate.now();

        RegistrationDate result = new RegistrationDate();

        assertNotNull(result);
        assertEquals(expected, result.getDate());
    }

    @Test
    void createRegistrationDate_Unsuccessfully_DifferentDate(){
        LocalDate expected = LocalDate.of(2020,3,2);

        RegistrationDate result = new RegistrationDate();

        assertNotNull(result);
        assertNotEquals(expected, result.getDate());
    }
}