package repositories;

import model.Family.Family;
import model.shared.FamilyId;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FamilyRepositoryTest {

    @Test
    void createAFamily_Successfully() {
        FamilyRepository familyRepository = new FamilyRepository();
        String familyName = "Silva";
        String adminId = "antonio@gmail.com";
        int expected = 1;
        Family expectedFamily = new Family(0, familyName, adminId);

        FamilyId aFamilyId = familyRepository.createFamily(familyName, adminId);

        assertEquals(expected, familyRepository.getFamiliesSize());
        assertEquals(expectedFamily, familyRepository.getFamilyById(aFamilyId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void createAFamily_Unsuccessfully_NullAndEmptyName(String name) {
        FamilyRepository familyRepository = new FamilyRepository();
        String familyName = name;
        String adminId = "antonio@gmail.com";

        assertThrows(IllegalArgumentException.class, () -> familyRepository.createFamily(familyName, adminId));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void createAFamily_Unsuccessfully_NullAndEmptyEmail(String email) {
        FamilyRepository familyRepository = new FamilyRepository();
        String familyName = "Silva";
        String adminId = email;

        assertThrows(IllegalArgumentException.class, () -> familyRepository.createFamily(familyName, adminId));
    }

}