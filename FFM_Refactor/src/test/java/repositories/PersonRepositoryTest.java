package repositories;

import model.shared.Email;
import org.junit.jupiter.api.Test;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.Month;

import static org.junit.jupiter.api.Assertions.*;

public class PersonRepositoryTest {


    @Test
    void ensurePersonIsCreated() {

        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        Email result;

        //act
        result = personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);

    }

    @Test
    void ensurePersonIsCreatedWithEmptyPhoneNumber() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "";
        Email result;

        //act
        result = personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);

    }

    @Test
    void ensurePersonIsCreatedWithBlankPhoneNumber() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = " ";
        Email result;

        //act
        result = personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertNotNull(result);
    }

    @Test
    void ensurePersonEmailEqualsIsPersonId() {

        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        Email expected = new Email(email);
        Email result;

        //act
        result = personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        //assert
        assertEquals(expected, result);

    }

    @Test
    void ensurePersonIsNotCreatedWhenPersonWithSameEmailAlreadyExists() {

        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "229654117";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916789456";
        personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber);

        String sameEmail = "antonio@gmail.com";
        String otherPersonName = "António";
        String otherVat = "229654118";
        String otherStreet = "Rua da Alegria";
        String otherLocation = "Porto";
        String otherPostalCode = "4679-876";
        LocalDate otherBirthDate = LocalDate.of(1994, Month.NOVEMBER, 1);
        String otherPhoneNumber = "916789456";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(sameEmail, otherPersonName, otherVat, otherStreet, otherLocation, otherPostalCode, otherBirthDate, otherPhoneNumber));
    }


    @Test
    void ensurePersonIsNotCreatedWhenInvalidVat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "1239845981";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidEmailFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio.gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidNameFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António12345";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidStreetFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = " ";
        String location = "Porto";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidLocationFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto12345";
        String postalCode = "4678-876";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }


    @Test
    void ensurePersonIsNotCreatedWhenInvalidPostalCodeFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-80998765";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidBirthDate() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-809";
        LocalDate birthDate = null;
        String phoneNumber = "916784345";

        //act + assert
        assertThrows(DateTimeException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

    @Test
    void ensurePersonIsNotCreatedWhenInvalidPhoneNumberFormat() {
        //arrange
        PersonRepository personRepository = new PersonRepository();
        String email = "antonio@gmail.com";
        String personName = "António";
        String vat = "228278112";
        String street = "Rua das Flores";
        String location = "Porto";
        String postalCode = "4678-809";
        LocalDate birthDate = LocalDate.of(1995, Month.NOVEMBER, 1);
        String phoneNumber = "1916784345";

        //act + assert
        assertThrows(IllegalArgumentException.class, () -> personRepository.createPerson(email, personName, vat, street, location, postalCode, birthDate, phoneNumber));
    }

}
