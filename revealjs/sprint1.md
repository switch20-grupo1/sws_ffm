## User Stories Distribution

<!-- .slide: style="font-size: 63%;" -->

|             Student | Sprint 1                                                        |           |
| ------------------: | --------------------------------------------------------------- | --------- |
|          _Grupo Um_ | [US001](#/US001) Create Standard Category                       | Completed |
|          _Grupo Um_ | US002 Get Standard Categories Tree                              | Completed |
| **Nuno Casteleira** | US010 Create Family                                             | Completed |
|   **Filipa Borges** | US011 Add Family Administrator                                  | Completed |
|     **João Vieira** | US101 Add Family Members                                        | Completed |
|  **Raquel Ribeiro** | [US104](#/US104) Get List Of Family Members And Their Relations | Completed |
| **Jaqueline André** | US105 Create a Relation Between Two Family Members              | Completed |
|          _Grupo Um_ | US110 Get Family Category Tree List                             | Completed |
|   **Ricardo Nunes** | US120 Create Family Cash Account                                | Completed |
|       **Ana Jorge** | US150 Get Profile Information                                   | Completed |
|      **Maria Luís** | US151 Add Email Account To Family Member Profile                | Completed |

---

## Links

- [Domain Model](#/domain-model)
- [Class Diagram](#/class-diagram)

---

## Domain Model

<!-- .slide: id="domain-model" -->

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
title Domain Model Sprint 1
hide methods

class Family {
  - familyId
  - name
  - registrationDate
  - administrator
  - personList
  - familyRelationshipList
}

Family "1" - "0..*" FamilyRelationship : has

class FamilyRelationship{
 - relationshipType
 - personId
}

class Person {
 - personId
 - name
 - birthDate

}

FamilyRelationship "1" - "1" Person : with Main User

class VAT {
 - VATnumber
}

class Email {
 - emailAddress
}

class Phone {
- phoneNumber
}

class Address {
 - street
 - location
 - postalCode
}


class Category {
 - categoryId
 - designation
 - childOf
 - isStandard
}


Family "1" -- "0..1" CashAccount : has

class CashAccount {
  - accountId
  - familyId
  - cashAmount
}

class FamilyCategory {
 - familyId
 - categoryId
}

Person "1" - "1" VAT : has
Person "1" - "0..*" Email : has
Person "1" - "0..*" Phone : has
Person "1" - "1" Address : has
VAT --[hidden] Email
VAT --[hidden] Phone
FamilyRelationship --[hidden] Address
Family "1" -u- "0..*" FamilyCategory : ^ has Family\nCategory
FamilyCategory "0..*" - "1" Category : is a kind of

@enduml
```

---

## Class Diagram

<!-- .slide: id="class-diagram" -->

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
scale max 550 height

class FamilyService{
- familyList
+ addFamily(name)
+ getFamiliesWithNoAdmin()
+ getFamilyById(familyId)
+ getRelationshipTypesList()
+ getFamilyMembersListWithoutAdmin(familyId)
+ existsRelationship(familyId,personId)
+ createRelationship(familyId,personId,relationshipType)
- getNextId
+ getFamilyRelationshipsList(familyId)
+ familyRelationshipsListToString(familyId)
}

class PersonService{
- personList
- familyService
- createPerson(idPerson, name, ...)
+ addPerson(name, birthDate, ...)
+ addAdmin(name, birthDate, ...)
+ getPersonById(personId)
- existsEmail()
+ addEmail
- getNextId()
}

class FamilyCashAccount{
- accountId
- familyId
- cashAmount
+ getAccountId()
+ getFamilyId()
}

class FamilyRelationship{
- relationshipType
- personId
+ checkIfRelationshipRefersToMember(personId)
+ getPersonId()
+ getFamilyRelationshipType()
+ equals()
}

enum FamilyRelationshipType{
Husband_Wife
Partner
Parent
Child
Sibling
Grandparent
Grandchild
Uncle_Aunt
Nephew_Niece
Cousin
- numericValue
+ valueOf(numericValue)
+ toString()
}


class Family {
- familyId
- name
- registrationDate
- administrator
- personList
- familyRelatioships
- vatList
- familyCategoryList
+ createFamily(name, registrationDate)
- validate(name)
- checkFormat(name)
+ getFamilyId()
+ addAdmin(person)
+ addMember(person)
+ hasAdministrator()
+ getPersonList()
+ getFamilyMembersList()
+ checkIfPersonExistsInFamily(personId)
+ getAdministrator()
+ getFamilyRelationshipsList()
+ addFamilyRelationship(relationship)
- existsVat(vatNumber)
+ addCategory(category)
+ existsCategory(other)
+ getNextFamilyCategoryListId()
+ getFamilyCategoryList()
}

class Person {
- personId
- name
- birthDate
- emailAddressList
+ getPersonId()
+ getVat()
- birthDateToString(birthDate)
- parseDate(birthDate)
+ addEmail(emailAddress)
+ getPersonName()
+ getEmailAddressList()
- validateName(name)
- checkFormat(name)
+ toString()
+ equals()
}

class VAT {
- number
+ createVAT()
- validate(vat)
- checkFormat(vat)
+ getVat()
+ toString()
}

class EmailAddress {
- email
- validate(email)
- checkFormat(email)
+ toString()
+ equals(other)
+ hasCode()
}

class PhoneNumber {
- number
+ createPhone()
- validate(phoneNumber)
- checkFormatphoneNumber)
+ toString()
}

class Address {
- street
- location
- postalCode
- validateStreet(street)
- validatePostalCode(postalCode)
- checkFormat(postalCode)
- validateLocation(location)
+ toString()
}


class Category {
- categoryId
- name
- parentId
- isStandard
+ createStandardRootCategory(name,categoryId)
+ createStandardCategory(name,categoryId,parentId)
+ createRootCategory(name,categoryId)
+ createCategory(name,categoryId,parentId)
- validate(name)
- checkFormat(String name)
+ getParentId()
+ getCategoryId()
+ getCategoryName()
+ hasParent()
+ toString()
+ equals(other)
+ hasCode()
}

class CategoryService {
- categoryList
- familyService
+ addStandardCategory(name,parentName)
+ addStandardCategory(name)
+ addCategory(name, parentName)
+ addCategory(name)
- existsCategory(other)
+ getSize()
+ getCategoryById(categoryId)
+ getStandardCategoryTree()
- appendChildCategories(unorderedList,parentCategoryId,orderedList)
+ getStandardCategoryTreeToString()
- getLevel(category)
+ getCategoryByName(name)
+ getNextId()
+ getNextFamilyId(familyService)
+ addFamilyCategory(familyService,name)
+ addFamilyCategory(familyService,name,parentName)
+ getFamilyCategoryTree(familyService)
+ getMergedFamilyCategoryTree(familyService)
- mergeFamilyCategoryAndStandardCategoryTree(familyService)
+ getFamilyCategoryByName(familyService,name)
}

class FamilyCashAccountService{
- familyCashAccountList
+ existsFamilyCashAccount(familyCashAccountList, newFamilyCashAccount)
+ getNextId()
}

CategoryService . FamilyService : knows
FamilyService . PersonService : knows
CategoryService "1" *-- "0..*" Category : contains
FamilyService "1" *-- "0..*" Family : contains
PersonService "1" *-- "0..*" Person : contains
Person "0..1" -d- "1" FamilyRelationship : fromFamilyAdmin
Family "0..1" -- "0..*" FamilyRelationship : exists
FamilyRelationship "1" - "1" FamilyRelationshipType : isOfType
Family .- FamilyCashAccountService : knows
FamilyCashAccount "0..*" -* "1" FamilyCashAccountService : < contains
PersonService -[hidden] VAT
Person "1" -u- "1" VAT : has ^
Person "1" - "0..*" PhoneNumber : has
Person "1" -l "1" Address : > has
VAT -[hidden] EmailAddress
Person "1" -u- "0..*" EmailAddress : has ^
@enduml
```

---

## US001

**Create Standard Category**

<!-- .slide: id="US001" -->
<!-- .slide: style="font-size: 65%;" -->

_As a system manager I want to create a standard category._

```language-plantuml
skinparam backgroundColor #fefefe
header LV_BL
:System Manager: as sysMan

(US001 To Create a Standard Category) as (US001)

left to right direction
sysMan --> (US001)
```

</br>

```language-plantuml
skinparam backgroundColor #fefefe
@startuml
header SSD
autonumber
actor "System Manager" as SM
participant ": Application" as App

SM -> App : Create a new category
activate SM
activate App
return ask category name
deactivate App

SM -> App : input category name
activate App

return inform success
deactivate App

deactivate SM
@enduml
```

---

### Analysis

<!-- .slide: style="font-size: 80%;" -->

- Shipped with the application, available to every user/family.
- Used to categorize a financial transaction.

</br>

| Attributes | Rules                                                          |
| ---------- | -------------------------------------------------------------- |
| categoryId | unique, required, integer, auto-incrementing                   |
| name       | required, alphanumeric, String                                 |
| parentId   | the id of a Category instance parent of this instance          |
| isStandard | a boolean to identify what categories are shipped with the App |

---

<!-- .slide: style="font-size: 85%;" -->

### Analysis (cont.)

Categories should have a unique identification across the app for two reasons:

- It prevents user errors choosing a category;
- It prevents repeated additions of the same referring category.

</br>

#### Parent and children categories

```language-bash
CATEGORIES
|-- Shopping (1)
|   |-- Groceries (1.1)
|   |   |-- Vegetables (1.1.1)
|   |   |-- Protein (1.1.2)
|   |   |-- Carbs (1.1.3)
|   |-- Clothes (1.2)
|-- Bills (2)
|   |-- Energy (2.1)
(...)
```

---

### Design

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
scale max 550 height
header SD
title Create a category
autonumber
actor "System Manager" as SM
participant ": UI" as UI
participant ": CreateCategory\nController" as CCC
participant ": Application" as App
participant "categoryService:\nCategoryService" as CS

SM -> UI : Create a new category
activate SM
activate UI
return ask category name
deactivate UI

SM -> UI : input category name
activate UI
UI -> CCC : createStandardCategory(name)
activate CCC

CCC -> App : getCategoryService()
activate App
CCC <--App: categoryService
deactivate App

CCC -> CS : addStandardCategory(name)

note left of CCC
There is a similar sequence action
based on <b>4</b> createCategory(name,parentId),
to create a child category
end note

activate CS
ref over CS
    addStandardCategory()
end ref
CS --> CCC: boolean
deactivate CS

CCC --> UI: boolean
deactivate CCC
return inform success
deactivate UI

deactivate SM
@enduml
```

---

### Design (cont.)

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
header ref
title addStandardCategory()
autonumber
participant ": CategoryService" as CS


[-> CS : addStandardCategory(name)
activate CS

CS --> newCat as "aCategory:\nCategory" ** : create(name, categoryId)

note left of CS
There is a similar sequence action
addStandardCategory(name,parentName),
to create a child category
end note

CS -> CS : existsCategory(aCategory)

opt !existsCategory(aCategory)
CS -> CS : add(aCategory)

end
[<-- CS: boolean
deactivate CS
@enduml
```

---

### Tests

<!-- .slide: style="font-size: 70%;" -->

- Designation

  - **Unit Test 1:** Throw an error when creating a category with empty string.

```java
  //CategoryTest.java
    @Test
    void createCategoryWithEmptyName() {
        CategoryService categoryService = new CategoryService();
        String designation = "";

        assertThrows(IllegalArgumentException.class, () -> categoryService.addStandardCategory(designation));
    }
```

---

### Tests (cont.)

<!-- .slide: style="font-size: 70%;" -->

- Category

  - **Unit Test 1:** Assert the creation of a new category.

  ```java
    //CategoryTest.java
    @Test
    void createStandardCategoryWithAName() {
        String designation = "TestCategory";
        int categoryId = 0; //categoryId is responsibility of CategoryService
        Category category = Category.createStandardCategory(designation, categoryId);
        String expected = "Category{categoryId=0, name='TestCategory', parentId=-1, isStandard=true}";
        String result;
        result = category.toString();
        assertEquals(expected, result);
    }
  ```

  - **Unit Test 2:** Do not create repeating categories.

  ```java
      //CategoryServiceTest.java
      @Test
      void addRepeatedCategory() {
          CategoryService categoryService = new CategoryService();
          categoryService.addStandardCategory("shopping");
          categoryService.addStandardCategory("shopping");
          int expected = 1;
          int result = categoryService.getSize();
          assertEquals(expected, result);
      }
  ```

---

### Tests

<!-- .slide: style="font-size: 70%;" -->

- Category

  - **Unit Test 3:** Assert the creation of a child categories.</br>(abridged from the code)

```language-java
  //CategoryService.java
  @Test
  void testToString() {
      CategoryService categoryService = new CategoryService();
      String root0 = "Shopping";
      String node00 = "Groceries";
      String node000 = "Vegetables";
      String node01 = "Clothes";
      String root1 = "Bills";
      categoryService.addStandardRootCategory(root0);
      categoryService.addStandardCategory(node00, root0);
      categoryService.addStandardCategory(node000, node00);
      categoryService.addStandardCategory(node01, root0);
      categoryService.addStandardRootCategory(root1);
      String expected = "|-- Shopping\n" +
                        "|   |-- Groceries\n" +
                        "|   |   |-- Vegetables\n" +
                        "|   |-- Clothes\n" +
                        "|-- Bills";
      String result = categoryService.getStandardCategoryTreeToString();
      //implemented do show categories in a tree-like string
      assertEquals(expected, result);
  }
```

---

### Comments

- Possible modifications to the implementation might be done in the future, so as
  to improve code reusability and simplicity, by implementing the use of
  interfaces and inheritance principles.

---

## US104

<!-- .slide: id="US104" -->
<!-- .slide: style="font-size: 68%;" -->

**Get List Of Family Members And Their Relations**

_As a family administrator, I want to get the list of family members and their relations._

```language-plantuml
skinparam backgroundColor #fefefe
header LV_BL
:Family Administrator: as famAdmin

(US104 To List Family Members and Their Relations) as (US104)

left to right direction
famAdmin --> (US104)
```

</br>

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
autonumber
header SSD
title Get list of family members and their relations
actor "Family Administrator" as FM
participant ": Application" as App

FM -> App : Get list of members and their relations
activate FM
activate App

App --> FM : Show list of members and their relations
deactivate App
deactivate FM
@enduml
```

---

### Analysis

<!-- .slide: style="font-size: 90%;" -->

- FamilyRelationship:

| Attributes       | Rules                        |
| ---------------- | ---------------------------- |
| relationshipType | required, enum type constant |
| personId         | unique, required, integer    |

</br>

<style type="text/css">
.tg .tg-c3ow{text-align:center;}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-c3ow" colspan="2">Relationship Types</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Husband/Wife</td>
    <td class="tg-0pky">Partner</td>
  </tr>
  <tr>
    <td class="tg-0pky">Parent</td>
    <td class="tg-0pky">Child</td>
  </tr>
  <tr>
    <td class="tg-0pky">Sibling</td>
    <td class="tg-0pky">Grandparent</td>
  </tr>
  <tr>
    <td class="tg-0pky">Grandchild</td>
    <td class="tg-0pky">Uncle/Aunt</td>
  </tr>
  <tr>
    <td class="tg-0pky">Nephew/Niece</td>
    <td class="tg-0pky">Cousin</td>
  </tr>
</tbody>
</table>

---

### Design

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
scale max 550 height
autonumber
header SD
title Get list of family members and their relations
actor "Family Administrator" as FA
participant ": UI" as UI
participant ": GetMembersAndRelationsController" as GMRC
participant ": Application" as App
participant " : FamilyService" as FS

activate FA
FA -> UI : Get list of members and their relations
activate UI
UI -> GMRC : getListOfMembersAndTheirRelations(familyId)
activate GMRC
GMRC -> App : getFamilyService()
activate App
App --> GMRC : familyService
deactivate App

GMRC -> FS : getFamilyRelationshipList(familyId)
Activate FS
ref over FS: getFamilyRelationshipList()
FS --> GMRC : listOfAllMembersAndRelations
Deactivate FS

GMRC --> UI : listOfAllMembersAndRelations
deactivate GMRC
UI --> FA : Show list of members and their relations
deactivate UI
@enduml
```

---

### Design (cont.)

```language-plantuml
@startuml
skinparam backgroundColor #fefefe
scale max 550 height
header ref
title getFamilyRelationshipList()
autonumber
participant " : FamilyService" as FS
participant " : Family" as F

[-> FS: getFamilyRelationshipList(int familyId)
activate FS

FS --> FS: getFamilyById(familyId)

FS -> F: getFamilyRelationshipList(familyId)
Activate F
F --> FS: listOfAllMembersAndRelations
Deactivate F

[<--FS : listOfAllMembersAndRelations
Deactivate FS
@enduml
```

---

### Tests

<!-- .slide: style="font-size: 70%;" -->

- **Unit Test 1:** Get a valid list of family relationships

```language-java
@Test
    void returnSuccessfullyFamilyRelationshipsList() {

        Family family = createMockDataToCreateRelationshipTesting();
        int familyId = family.getFamilyId();
        familyService.createRelationship(familyId, 0, FamilyRelationshipType.Husband_Wife);
        familyService.createRelationship(familyId, 1, FamilyRelationshipType.Child);
        List<FamilyRelationship> relationshipList = familyService.getFamilyRelationshipsList(familyId);

        int expectedListSize = 2;
        int actualListSize = relationshipList.size();

        assertEquals(expectedListSize, actualListSize);
        assertNotNull(relationshipList);
    }
```

---

### Tests (cont.)

<!-- .slide: style="font-size: 70%;" -->

- **Unit Test 2:** Get a valid String list of family relationships

```language-java
    @Test
  void returnSuccessfullyFamilyRelationshipsStringList() {

      Family family = createMockDataToCreateRelationshipTesting();
      int familyId = family.getFamilyId();
      familyService.createRelationship(familyId, 0, FamilyRelationshipType.Husband_Wife);
      familyService.createRelationship(familyId, 1, FamilyRelationshipType.Child);
      familyService.createRelationship(familyId, 2, FamilyRelationshipType.Child);
      List<String> relationshipStringListResult = familyService.familyRelationshipsListToString(familyId);

      List<String> expectedStringList = Arrays.asList("Maria - Husband_Wife", "José - Child", "Rita - Child");

      int expectedListSize = 3;
      int actualListSize = relationshipStringListResult.size();

      assertEquals(expectedStringList, relationshipStringListResult);
      assertEquals(expectedListSize, actualListSize);
      assertNotNull(relationshipStringListResult);
  }
```

---

### Tests (cont.)

<!-- .slide: style="font-size: 70%;" -->

- **Unit Test 3:** Get an empty list

```language-java
   @Test
   void returnAnEmptyFamilyRelationshipsList() {

       Family family = createMockDataToCreateRelationshipTesting();
       int familyId = family.getFamilyId();

       List<FamilyRelationship> relationshipList = familyService.getFamilyRelationshipsList(familyId);

       int expectedListSize = 0;
       int actualListSize = relationshipList.size();

       assertEquals(expectedListSize, actualListSize);
   }
```

---

### Tests (cont.)

<!-- .slide: style="font-size: 70%;" -->

- **Unit Test 4:** Throw exception when the family doesn't exist

```language-java
@Test
   void returnFamilyRelationshipsListWithFamilyIdThatDoesNotExist() {

       Family family = createMockDataToCreateRelationshipTesting();

       assertThrows(IllegalArgumentException.class, () -> {
           familyService.getFamilyRelationshipsList(1);
       });
   }
```

---

### Comments

Thinking of further features that can be related to this user story in the future would be about editing the list of relationships.

It can be also useful to implement a Data Transfer Object (DTO) class.

---

### Applied Patterns

<!-- .slide: style="font-size: 80%;" -->

<style type="text/css">
.tg .tg-0lax{text-align:center;}
</style>
</style>
<table class="tg">
<thead>

</thead>
<tbody>
  <tr>
    <td class="tg-0lay">Single Responsibility Principle</td>
    <td class="tg-0lay">Information Expert</td>
  </tr>
  <tr>
    <td class="tg-0lay">Pure Fabrication</td>
    <td class="tg-0lay">Creator</td>
  </tr>
  <tr>
    <td class="tg-0lay">Controller</td>
    <td class="tg-0lay">Low Coupling</td>
  </tr>
  <tr>
    <td class="tg-0lay">High Cohesion</td>
    <td class="tg-0lay"></td>
  </tr>
</tbody>
</table>

---

## Group One thanks your comments

### The-one

<!-- .element: class="h3" style="text-align:right" -->

[us001]: #/US001
[us104]: #/US104
