## User Stories Distribution

<!-- .slide: style="font-size: 53%;" -->

| Student                       | Sprint 2                                                       |           |
| ----------------------------- | -------------------------------------------------------------- | --------- |
| João Vieira                   | US106 Change Relation Between Family Members                   | Completed |
| Jaqueline André               | US111 Add Category To Family Category Tree                     | Completed |
| Maria Luís & Nuno Casteleira  | US130 Transfer Family Cash Money To Family Member              | Completed |
| Filipa Borges & Ricardo Nunes | US135 Check Balance Family Cash Account or Given Family Member | Completed |
| Filipa Borges & Ricardo Nunes | US170 Create Personal Cash Account                             | Completed |
| Ana Jorge & Raquel Ribeiro    | US171 Add Personal Bank Account                                | Completed |
| Filipa Borges & Ricardo Nunes | US172 Add Bank Savings Account                                 | Completed |
| Maria Luís & Nuno Casteleira  | [US173](#/US173) Add Credit Card Account                       | Completed |
| Ana Jorge & Raquel Ribeiro    | US180 Transfer My Cash To Other Family Member                  | Completed |
| Maria Luís & Nuno Casteleira  | US181 Register Payment Using Cash Account                      | Completed |
| João Vieira                   | [US185](#/US185) Check Balance Of One Of My Accounts           | Completed |
| Jaqueline André               | US186 Get Movements Between Two Dates                          | Completed |
| Maria Luís & Nuno Casteleira  | US188 Check Balance Of One of My Children's Cash Account       | Completed |

---

## Domain Model

<!-- .slide: id="domain-model" -->

```language-plantuml
@startuml
scale max 550 height
title Domain Model Sprint 2
hide methods

class Family {
  - familyId
  - name
  - registrationDate
  - administrator
  - personList
  - familyRelationships
  - vatList
  - familyCategoryList
}

Family "1" - "0..*" FamilyRelationship : has

class FamilyRelationship{
 - personId
 - relationshipType
}

class Person {
 - personId
 - name
 - birthDate
 - emailAddressList
 - Address
 - phoneNumber
 - VAT
}

FamilyRelationship "0..*" - "2" Person : between


class PersonalCashAccount {
  - accountId
  - accountOwner
  - designation
}

class BankAccount {
  - accountId
  - accountOwner
  - designation
}
class BankSavingsAccount {
  - accountId
  - accountOwner
  - designation
}
class CreditCardAccount {
  - issuer
  - cardToken
  - creditLimit
  - interestRate
}
class FamilyCashAccount {
   - accountId
   - accountOwner
   - designation
 }

Transaction -- FamilyCashAccount: has list of <
Transaction -- PersonalCashAccount: has list of <
Transaction -- CreditCardAccount: has list of <
Transaction -- BankAccount: has list of <
Transaction -- BankSavingsAccount: has list of <

FamilyCashAccount "0..1" -- "1" Family : has <
PersonalCashAccount "0..1" -- "1" Person : has <
BankAccount "0..*" -- "1" Person : has <
BankSavingsAccount "0..*" -- "1" Person : has <
CreditCardAccount "0..*" -- "1" Person : has <

class Category {
 - categoryId
  - name
  - parentId
  - isStandard
}

Family "0..*" -- "0..*" Category : has FamilyCategories

abstract class Transaction {
    - transactionId
    - description
    - category
    - timestamp
    - customTimestamp
    - amount
    - postBalance
}

class Payment {
    - entityDescription
}

class Transfer {
- otherTransfer
- otherAccountOwner
}

class MonetaryValue {
- value
- currency
}

Transaction <|-u- Payment
Transaction <|-u- Transfer
MonetaryValue -* Transaction::amount
@enduml
```

---

<!-- .slide: id="class-diagram" -->

```language-plantuml
@startuml
hide fields

scale max 700 height
scale max 700 width

title Class Diagram Sprint 2

class Family {
}

class FamilyService {
}

class Person {
}

class PersonService {
}

class PersonalCashAccount {
}

interface Transferable {
}

interface Payable {
}

class BankAccount {
}

class BankSavingsAccount {
}

class CreditCardAccount {
}

class FamilyCashAccount {
}

class AccountOwner {
}

interface Ownership {
}

class Category{
}

class CategoryService {
}

abstract class Transaction {
}

class TransactionService {
}

class Payment {
}

class Transfer {
}

class MonetaryValue {
}

abstract class Account {
}

class AccountService {
}

class FamilyRelationship {
}

Enum FamilyRelationshipType {
}

Account <|-u- FamilyCashAccount
Account <|-u- PersonalCashAccount
Account <|-u- BankAccount
Account <|-u- BankSavingsAccount
Account <|-u- CreditCardAccount

Transaction --* FamilyCashAccount: has list of <
Transaction --* PersonalCashAccount: has list of <
Transaction --* CreditCardAccount: has list of <
Transaction --* BankAccount: has list of <
Transaction --* BankSavingsAccount: has list of <

AccountOwner .down.|> Ownership
Ownership "1" -left- "1" Family
Ownership "1..*" -right- "1" Person

Category "0..*" - "1..*" Family : has FamilyCategories
Category "0..*" --* "1" CategoryService : contains <

Transaction "0..*" - "1" TransactionService : contains

Payable <|-[dashed]- PersonalCashAccount
Transferable <|-[dashed]- PersonalCashAccount
Transferable <|-[dashed]- FamilyCashAccount

Family "0..*" -up-* "1" FamilyService : contains
Person "0..*" -* "1" PersonService : contains
Account "0..*" -* "1" AccountService : contains

Transaction <|-u- Payment
Transaction <|-u- Transfer

MonetaryValue -* Transaction::amount
Account "1..*" -- "1" AccountOwner
Family -[hidden] Person

Family "1" -- "0..*" FamilyRelationship : has
Person "2" -- "0..*" FamilyRelationship : between
FamilyRelationship "1" - "1" FamilyRelationshipType : isOfType
Ownership -[hidden] FamilyRelationship
@enduml
```

---

## US173

<!-- .slide: id="US173" -->
<!-- .slide: style="font-size: 55%;" -->

_As a family member, I want to add a credit card account I have._

<br>

```language-plantuml
@startuml
Header SSD
autonumber
title Create a credit card account
actor "Family Member" as FM
participant ": Application" as App

FM -> App : Create credit card account
activate FM

activate App
App --> FM : ask information
deactivate App

FM -> App : inputs information
activate App
App --> FM: informs sucess
deactivate App
deactivate FM

@enduml

```

---

### Analysis

<!-- .slide: style="font-size: 70%;" -->

- A Credit Card Account is a kind of Account, having the same generic properties as any other kind.
- It has their own specificities.

</br>
</br>

<style type="text/css">
.tg .tg-4sxv{border-color:black;color:#9b9b9b;}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Attributes</th>
    <th class="tg-0pky">Rules</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-4sxv">accountId</td>
    <td class="tg-4sxv">Generated, UUID</td>
  </tr>
  <tr>
    <td class="tg-4sxv">accountOwner</td>
    <td class="tg-4sxv">Required, Ownership</td>
  </tr>
  <tr>
    <td class="tg-4sxv">currency</td>
    <td class="tg-4sxv">Required, Currency</td>
  </tr>
  <tr>
    <td class="tg-4sxv">transactionList</td>
    <td class="tg-4sxv">List of Transactions</td>
  </tr>
  <tr>
    <td class="tg-4sxv">designation</td>
    <td class="tg-4sxv">String (a default one is provided if empty)</td>
  </tr>
  <tr>
    <td class="tg-0pky">issuer</td>
    <td class="tg-0pky">Optional, String</td>
  </tr>
  <tr>
    <td class="tg-0pky">creditLimit</td>
    <td class="tg-0pky">Required, MonetaryValue</td>
  </tr>
  <tr>
    <td class="tg-0pky">cardToken</td>
    <td class="tg-0pky">Required, integer, last 4 digits of the card</td>
  </tr>
  <tr>
    <td class="tg-0pky">interestRate</td>
    <td class="tg-0pky">Optional, BigDecimal</td>
  </tr>
</tbody>
</table>

---

### Design

```language-plantuml
@startuml
header SD
title Add a Credit Card Account
scale max 550 height
autonumber
actor "Family Member" as FM
participant ": UI" as UI
participant ": CreateCreditCardAccount\nController" as ccac
participant ": Application" as App
participant ": AccountService" as AS

activate FM
FM -> UI : Create credit card account
activate UI
UI --> FM : Ask information
deactivate UI

FM -> UI : inputs information
activate UI
UI -> ccac : CreateCreditCardAccount\n(issuer,currency,creditLimit,\ncardToken,designation)
activate ccac

ccac -> App : getAccountService()
activate App
App --> ccac : accountService
deactivate App

ccac -> App : getLoggedPerson()
activate App
App --> ccac : person
deactivate App

ccac -> AS : createCreditCardAccount(person,issuer,\ncreditLimit,cardToken,designation)
activate AS
  ref over AS
  createCreditCardAccount()
  end ref
AS --> ccac : inform result
deactivate AS
ccac --> UI : inform result
deactivate ccac
UI --> FM : inform result
deactivate UI
deactivate FM
@enduml
```

---

### Design (cont.)

```language-plantuml
@startuml
header ref
title createCreditCardAccount()
participant ": AccountService" as AS

[-> AS : createCreditCardAccount(person,issuer,\ncreditLimit,cardToken,designation)
activate AS

AS --> PCS as "aCreditCardAccount:\nCreditCardAccount"** : create()

AS -> AS : add(aCreditCardAccount)

[<-- AS : inform result
deactivate AS

@enduml
```

---

### Tests

<!-- .slide: style="font-size: 70%;" -->

| Positive Test Cases ✅                                            | Negative Test Cases ❌                                         |
| ----------------------------------------------------------------- | -------------------------------------------------------------- |
| Create a CC Account Successfuly                                   | Try to create a CC Account with an invalid Credit Limit (zero) |
| Create a second CC Account                                        | Try to create a CC Account with a negative Credit Limit        |
| Create a CC account with no currency </br>_(at controller level)_ | Try to create credit card account with invalid card token      |
| Create a CC account with invalid currency _(at controller level)_ | Try to create credit card account with invalid card issuer     |

---

### Comments

<!-- .slide: style="font-size: 80%;" -->

- The use of an abstract class reduces code duplication.

- At the moment, a credit card account can only be owned by one family member.
  As a possible improvement to the implementation of this US, the team could, in
  the future, allow for the account to have more than one owner.

- The balance probably will need to be calculated differently than other accounts.
  For now, it is being calculated by subtracting Transactions to the Credit Limit.

- The Design used in this User Story allows the implementation of Transferable,
  Payable and other interfaces of Transaction uses.

---

## US185

<!-- .slide: id="US185" -->

<!-- .slide: style="font-size: 68%;" -->

**Check the balance of one of my accounts**

_As a family member, I want to check the balance of one of my accounts._

</br>

```language-plantuml
@startuml
autonumber
header SSD
title Check the balance of one of my accounts
actor "Family Member" as FM
participant ": Application" as App

FM -> App : Check account balance
activate FM
activate App
App --> FM : Asks to select one of the accounts
deactivate App

FM -> App : Select desired account
activate App
App --> FM : Show respective account balance
deactivate App
deactivate FM

@enduml
```

---

### Analysis

<!-- .slide: style="font-size: 90%;" -->

- An account object has the following attributes:

</br>

</br>

| Attributes      | Rules                                                                        |
| --------------- | ---------------------------------------------------------------------------- |
| accountId       | Unique, UUID                                                                 |
| accountOwner    | Required, Ownership                                                          |
| currency        | Required, Currency                                                           |
| transactionList | List of transactions                                                         |
| description     | String, a default description is provided if left blank by the family member |

---

### Design

```language-plantuml
@startuml
scale max 550 height
header SD
title Check the balance of one of my accounts
autonumber
actor "Family Member" as FM
participant ": UI" as UI
participant ": CheckAccountBalance\nController" as ctrl
participant ": Application" as app
participant ": AccountService" as CS

activate FM

FM -> UI : Check account balance
activate UI

UI -> ctrl : getListOfAccountsOfThisOwner()
activate ctrl

ctrl -> app : getLoggedPerson()
activate app
app --> ctrl : person
deactivate app

ctrl -> app : getAccountService()
activate app
app --> ctrl : accountService
deactivate app

ctrl -> CS : getListOfAccountsOfThisOwner(person)
activate CS
  ref over CS
  getListOfAccountsOfThisOwner()
  end ref
CS --> ctrl : accountDTOList
deactivate CS
ctrl --> UI : accountDTOList
deactivate ctrl

UI --> FM : Asks to select one of the account's
deactivate UI

FM -> UI : Select desired account
activate UI
UI -> ctrl : getAccountBalance(accountDTO)
activate ctrl


ctrl -> app : getAccountService()
activate app
app --> ctrl : accountService
deactivate app

ctrl -> CS : getAccountBalance(accountDTO)
activate CS
  ref over CS
  getAccountBalance()
  end ref
CS --> ctrl : monetaryValueDTO
deactivate CS


ctrl --> UI : monetaryValueDTO
deactivate ctrl
UI --> FM : Show respective account balance
deactivate UI
deactivate FM

@enduml

```

---

### Design(Cont.)

```language-plantuml
@startuml
autonumber
header ref
title getListOfAccountsOfThisOwner()
participant ": AccountService" as AS
participant "accountMapper:\nAccountMapper" as AOM
participant "accountDTO:\nAccountDTO" as ADTO

-> AS : getListOfAccountsOfThisOwner(person)
activate AS

opt hasAccount(person)
loop for each account in accountList

AS --> AOM* : create(account)
activate AOM
AOM --> ADTO* : create(accountId, designation)
return accountDTO
AS ->AS : add(accountDTO)
end
end

<-- AS : accountDTOList
deactivate AS

@enduml
```

---

### Design (cont.)

```language-plantuml
@startuml
autonumber
header ref
title getAccountBalance()
participant ": AccountService" as ACS
participant ": Account" as Acc
participant "monetaryValueMapper :\n MonetaryValueMapper" as MVP
participant "monetaryValueDTO :\n monetaryValueDTO" as MVDTO

[-> ACS : getAccountBalance(accountDTO)
activate ACS

ACS -> ACS : getAccountById(accountID)

ACS -> Acc : getBalance()
activate Acc
Acc --> ACS : monetaryValue
deactivate Acc
ACS --> MVP **: create(monetaryValue)
activate MVP
MVP --> MVDTO **: create(value, currency)

MVP --> ACS  :monetaryValueDTO
deactivate MVP

[<-- ACS : monetaryValueDTO
deactivate ACS

@enduml
```

---

### Tests

<!-- .slide: style="font-size: 70%;" -->

| Positive Test Cases ✅                                                                                                                       |
| -------------------------------------------------------------------------------------------------------------------------------------------- |
| Get the balance of a cash account successfully.                                                                                              |
| Get the balance of a bank account successfully when the user has a bank account and a personal cash account.                                 |
| Get the balance of a bank savings account successfully when the user has a bank account, a personal cash account and a bank savings account. |

</br>

| Negative Test Cases ❌                           |
| ------------------------------------------------ |
| An Empty Account List should throw an exception. |

<!-- .element: style="margin: 0; width:100%" -->

---

### Comments

- The use of abstract classes reduces code duplication.

- One possibility to improve this functionality is to return all balances of all accounts the owner has.
  As we are using a DTO, it can easily accomodate a list of all accounts and their balances.

- Similarly, because of the DTO strategy, other informations of the account can be sent without too much of a change on the functionality point of view (Open/Closed Principle).

---

## Group One thanks your comments

</br>
</br>
</br>

### #The-one 🥇

<!-- .element: class="h3" style="text-align:right" -->

[us173]: #/US173
[us185]: #/US185
